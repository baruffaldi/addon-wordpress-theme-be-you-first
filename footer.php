
<hr class="low" />

<!-- footer ................................. -->
<div id="footer">
	<p><strong>&copy; Copyright <?php echo date("Y")." "; bloginfo('name'); ?>. All rights reserved.</strong><?php wp_loginout(); ?><br />
	<strong>Developed by <a href="http://www.kimera-lab.com">Kimera Team</a>.</strong></p>
	<?php do_action('wp_footer'); ?>

</div> <!-- /footer -->

</div> <!-- /container -->

</body>

</html>