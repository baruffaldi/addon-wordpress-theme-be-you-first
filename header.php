<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php require_once get_template_directory()."/BX_functions.php"; ?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
			<title><?php bloginfo('name'); wp_title(); ?></title>
			<meta name='metadata'   content='Be-You :: Young Festival' />
			<meta name='subject'    content='Festival dell` arte in Liguria' />
			<meta name='author'     content='Kimera Team for Be-You' />
			<meta name='publisher'  content='www.kimera-lab.com' />
			<meta name='date'       content='30apr2006' />
			<meta name='form'       content='xhtml' />
			<meta http-equiv='Keywords' content='Be-you, Festival, Liguria, Genova' />
			<meta http-equiv='Description' content='Festival dell` arte in Liguria' />
			<meta http-equiv="Content-Type" content="<?php bloginfo('charset'); ?>" />
			<meta name="generator" content="WordPress <?php bloginfo('version'); ?>" />
			<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen, projection" />
			<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
			<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
			<link rel="alternate" type="application/atom+xml" title="Atom 0.3" href="<?php bloginfo('atom_url'); ?>" />
			<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
			<?php wp_head();?>
	</head>

	<body>

		<div id="container"<?php if (is_page() && !is_page("archives")) echo " class=\"singlecol\""; ?>>
				<div id="header">
					<a href="<?php bloginfo('url'); ?>"></a>
				</div>

				<div id="navigation">&nbsp;&nbsp;&nbsp;<a href="http://www.be-you.org" <?php if (is_home() && $_SERVER['PHP_SELF'] != "/wp-content/plugins/falbum/wp/album.php" ) echo 'class="selected"';?>>HOME</a> | <?php
							$pages = BX_get_pages();
							if ($pages) {
								foreach ($pages as $page) {
									$page_id = $page->ID;
									$page_title = $page->post_title;
									$page_title = strtoupper($page_title);
									$page_name = $page->post_name;
									if ($page_name == "archives") {
										(is_page($page_id) || is_archive() || is_search() || is_single())?$selected = ' class="selected"':$selected='';
										echo "<a href=\"".get_page_link($page_id)."\" ".$selected.">ARCHIVIO</a> | \n";
									}
									elseif ($page_name == "galleria") {
										($_SERVER['PHP_SELF'] == "/wp-content/plugins/falbum/wp/album.php")?$selected = ' class="selected"':$selected='';
										echo "<a href=\"http://www.be-you.org/wp-content/plugins/falbum/wp/album.php\" ".$selected.">GALLERIA</a> | \n";
									}
									elseif ($page_name == "podcast") {
										(is_page($page_id))?$selected = ' class="selected"':$selected='';
										echo "<a href=\"".get_page_link($page_id)."\" ".$selected.">PODCAST</a>\n";
									}
									else {
										(is_page($page_id))?$selected = ' class="selected"':$selected='';
										echo "<a href=\"".get_page_link($page_id)."\" ".$selected.">$page_title</a> | \n";
									}
								}
							}
						?><a href="/?feed=rss2" title="RSS2"><img src="/wp-content/themes/Be-You/images/rss.jpg" alt="RSS2" /></a>
			</div>

			<hr class="low" />
