<hr class="low" />

<!-- subcontent ................................. -->
<div id="subcontent">

<?php /**
       * Pages navigation. Disabled by default because all new pages are added
       * to the main navigation.
       * If enabled: Blix default pages are excluded by default.
       *
?>
	<h2><em>Pages</em></h2>
	<ul class="pages">
	<?php
		$excluded = BX_excluded_pages();
		wp_list_pages('title_li=&sort_column=menu_order&exclude='.$excluded);
	?>
	</ul>

<?php */ ?>

<?php if (is_home()) { ?>

	<?php
	/**
	 * If a page called "about_short" has been set up its content will be put here.
	 * In case that a page called "about" has been set up, too, it'll be linked to via 'More'.
	 
	$pages = BX_get_pages('with_content');
	if ($pages) {
		foreach ($pages as $page) {
			$page_id = $page->ID;
   			$page_title = $page->post_title;
   			$page_name = $page->post_name;
   			$page_content = $page->post_content;

   			if ($page_name == "about") $more_url = '<a href="'.get_page_link($page_id).'" class="more">More</a>';
   			if ($page_name == "about_short") {
   				$about_title = $page_title;
   				$about_text = BX_remove_p($page_content);
   			}
		}
		if ($about_text != "") {
			echo "<h2><em>".$about_title."</em></h2>\n";
			echo "<p>".$about_text;
			if ($more_url != "") echo " ".$more_url;
			echo "</p>\n";
		}
	}
	?>
	
<?php endif;*/ ?>

 	<h2><em>Categorie</em></h2>

	<ul class="categories">
	<?php wp_list_cats('sort_column=name&hide_empty=0'); ?> 
	</ul>
                   
    <h2><em>Live Shoutbox</em></h2>
	<?php jal_get_shoutbox(); ?>


	<h2><em>WebChat</em></h2>
<script language="javascript" src="http://www.userplane.com/chatlite/medallion/chatlite.cfm?
DomainID=8673f0fe14384f9b70668e0d86ef79a3"></script><noscript>You must have JavaScript enabled to use <a href="http://www.userplane.com" title="Userplane" target="_blank">Userplane Chat</a>. <a href="http://www.userplane.com/chatlite/directory/detail.cfm?guid=C87B5F4E%2DD811%2D4116%2DA737%2DF7174B041522" title="Userplane">Click to view Be%20You%20Young%20Festivalís Chat Detail</a>. <a href="http://www.userplane.com/webchat" title="Webchat" target="_blank">Userplane Webchat</a>&trade;</noscript>


           
	<h2><em>Galleria</em></h2>

	<ul class="categories">
	<?php get_flickrRSS(); ?> 
	</ul>



	<h2><em>Sondaggio</em></h2>

                    <?php jal_democracy(); ?>

	<h2><em>Links</em></h2>

	<ul class="links">
	<?php get_links('-1', '<li>', '</li>', '', 0, 'name', 0, 0, -1, 0); ?>
	</ul>
                    
	<h2><em>Feeds</em></h2>

	<ul class="feeds">
	<li><a href="<?php bloginfo_rss('rss2_url'); ?> ">Entries (RSS)</a></li>
	<li><a href="<?php bloginfo_rss('comments_rss2_url'); ?> ">Comments (RSS)</a></li>
	</ul>

	<h2><em>Powered by</em></h2>
	<a href="http://www.wordpress.org/"><img src="/wp-content/themes/Be-You/images/wp.png" alt="WordPress" /></a><br />
	<a href="http://validator.w3.org/check?uri=http://www.be-you.org/&charset=iso-8859-1&doctype=Inline"><img src="/wp-content/themes/Be-You/images/xhtml.png" alt="XHTML" /></a><br />
	<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.be-you.org/"><img src="/wp-content/themes/Be-You/images/css.png" alt="CSS" /></a><br />

	<!-- Inizio Codice Shinystat -->
		<script type="text/javascript" language="JavaScript" SRC="http://codice.shinystat.com/cgi-bin/getcod.cgi?USER=Nicodemusx"></script>
		<noscript>
			<p style=""><A HREF="http://www.shinystat.com" target="_top">
			<IMG SRC="http://www.shinystat.com/cgi-bin/shinystat.cgi?USER=Nicodemusx" ALT="ShinyStat" BORDER="0"></A></p>
		</noscript>
	<!-- Fine Codice Shinystat -->

<?php } ?>

<?php if (is_single()) { ?>

	<h2><em>Calendar</em></h2>

	<?php get_calendar() ?>

	<h2><em>Most Recent Posts</em></h2>

	<ul class="posts">
	<?php BX_get_recent_posts($p,10); ?>
	</ul>

	<h2><em>Powered by</em></h2>
	<a href="http://www.wordpress.org/"><img src="/wp-content/themes/Be-You/images/wp.png" alt="WordPress" /></a><br />
	<a href="http://validator.w3.org/check?uri=http://www.be-you.org/&charset=iso-8859-1&doctype=Inline"><img src="/wp-content/themes/Be-You/images/xhtml.png" alt="XHTML" /></a><br />
	<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.be-you.org/"><img src="/wp-content/themes/Be-You/images/css.png" alt="CSS" /></a><br />

	<!-- Inizio Codice Shinystat -->
		<script type="text/javascript" language="JavaScript" SRC="http://codice.shinystat.com/cgi-bin/getcod.cgi?USER=Nicodemusx"></script>
		<noscript>
			<p style=""><A HREF="http://www.shinystat.com" target="_top">
			<IMG SRC="http://www.shinystat.com/cgi-bin/shinystat.cgi?USER=Nicodemusx" ALT="ShinyStat" BORDER="0"></A></p>
		</noscript>
	<!-- Fine Codice Shinystat -->


<?php } ?>


<?php if (is_page("archives") || is_archive() || is_search()) { ?>

	<h2><em>Calendar</em></h2>

	<?php get_calendar() ?>

	<?php if (!is_page("archives")) { ?>

		<h2><em>Posts by Month</em></h2>

		<ul class="months">
		<?php get_archives('monthly','','','<li>','</li>',''); ?>
		</ul>

	<?php } ?>

	<h2><em>Posts by Category</em></h2>

	<ul class="categories">
	<?php wp_list_cats('sort_column=name&hide_empty=0'); ?> 
	</ul>
	<h2><em>Powered by</em></h2>
	<a href="http://www.wordpress.org/"><img src="/wp-content/themes/Be-You/images/wp.png" alt="WordPress" /></a><br />
	<a href="http://validator.w3.org/check?uri=http://www.be-you.org/&charset=iso-8859-1&doctype=Inline"><img src="/wp-content/themes/Be-You/images/xhtml.png" alt="XHTML" /></a><br />
	<a href="http://jigsaw.w3.org/css-validator/validator?uri=http://www.be-you.org/"><img src="/wp-content/themes/Be-You/images/css.png" alt="CSS" /></a><br />

	<!-- Inizio Codice Shinystat -->
		<script type="text/javascript" language="JavaScript" SRC="http://codice.shinystat.com/cgi-bin/getcod.cgi?USER=Nicodemusx"></script>
		<noscript>
			<p style=""><A HREF="http://www.shinystat.com" target="_top">
			<IMG SRC="http://www.shinystat.com/cgi-bin/shinystat.cgi?USER=Nicodemusx" ALT="ShinyStat" BORDER="0"></A></p>
		</noscript>
	<!-- Fine Codice Shinystat -->
	<?php } ?>


</div> <!-- /subcontent -->